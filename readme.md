![enter image description here](img/img1.JPG)
## RIGHI LIBRARY


 - As Library's Admin, after registering I can do login with Username
   and Password.
 - As Library's Admin I can refer to book's list.
 - As Library's Admin I can insert other books on list.
 - As Library's Admin I can revise wrong information like title, writer,
   etc. .
 - As Library's Admin I can manage a loan of a book (Availability,
   Addition, Updating and Deleting).
 - As Library's Admin I can insert a loan start/end date.
 - As Library's Admin I can verify all books reserved.
 - As Library's Admin I can check the position of the book on the shelf.
 - As Library's Admin I can manage user's profile.

----------

 - The user can do the registration using his credentials and then he can acess into the system with email e password.
 - The user can check the book's list and verify the availabilty.
 - The user can reserve a book/s.
 - The user can check the book reserve status.
 


----------
Diagramma delle classi � Class Diagram

![enter image description here](img/RighiLibraryUML.jpg)

----------
Use Case Diagram

![enter image description here](img/UseCaseDiagram2.jpg)

----------


![Software gestionale della biblioteca Augusto Righi](img/Software_gestionale_della_biblioteca_Augusto_Righi-001.jpg)

![Software gestionale della biblioteca Augusto Righi](img/Software_gestionale_della_biblioteca_Augusto_Righi-002.jpg)

![Software gestionale della biblioteca Augusto Righi](img/Software_gestionale_della_biblioteca_Augusto_Righi-003.jpg)

![Software gestionale della biblioteca Augusto Righi](img/Software_gestionale_della_biblioteca_Augusto_Righi-004.jpg)



----------
Diagramma-lavoro

![enter image description here](img/Diagramma-lavoro.jpg)
----------


diagramma-flusso_workflow

![enter image description here](img/diagramma-flusso_workflow.jpg)
----------


Prodotto-di-progetto

![enter image description here](img/Prodotto-di-progetto.jpg)

----------