CREATE DATABASE  IF NOT EXISTS `righilibrary` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `righilibrary`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: righilibrary
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL,
  `isbn` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `writer` varchar(45) NOT NULL,
  `publisher` varchar(45) NOT NULL,
  `position` varchar(45) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,3,'9788807903014','Eneide','Virgilio Marone Publio','Feltrinelli','1A',0),(2,5,'9788820302092','La Divina Commedia','Dante Alighieri','Feltrinelli','1A',0),(3,1,'9788804665281','Gomorra','Roberto Saviano','Mondadori','2A',0),(4,1,'9788839717184','La mia vita in cucina','Antonella Clerici','Mondadori','3A',0),(5,2,'9788804683476','Cosi Parlo Bella Vista','Luciano De Crescenzo','Mondadori','1A',0),(6,2,'9788822704610','Il conte di Montecristo','Alexandre Dumas','Newton Compton','2A',0),(7,1,'9788869880070','Game book','Elisa Pellacani','Consulta','3A',0),(8,4,'9788893814515','Harry Potter e la camera dei segreti','jk ROWLING','Salani Editore','1B',0),(9,2,'9788867158171','Harry Potter e il Principe Mezzosangue','jk ROWLING','Salani Editore','1B',0),(10,6,'9788854190627','Guerra e pace','Lev Nikolaevic Tolstoj','Newton Compton','2A',0),(11,8,'9788867180271','Il libretto rosso di Mao','TSE TSUNG MAO ','Red Star Press','1A',0);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-26 17:20:28
