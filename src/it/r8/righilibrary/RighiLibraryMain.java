package it.r8.righilibrary;
import it.r8.righilibrary.repository.*;
import java.util.Scanner;
import java.util.List;

import it.r8.righilibrary.domain.*;
public class RighiLibraryMain {
public static final int PASSWORD_LENGTH = 12;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		showLoginMenu();
	}
		
	public static void showLoginMenu() {	
		try (Scanner input = new Scanner(System.in)) {
			String c= null ;
			boolean registration=false;
			

			System.out.print("|    Biblioteca Righi     |\n"
					+        "|_________________________|\n"
					+		 "|      1 Registrati       |\n"
					+ 		 "|      2 Login            |\n"
					+        "|      3 Esci             |\n"
					+        "|_________________________|\n"); 
			
			while (input.hasNextLine()){
				
				c=input.nextLine();
				
				switch(c) {
					case "1": 
						UserRepository userRepository = new MysqlUserRepository();
						User user= new User();
						System.out.print("Nome : ");  
						user.setName(input.nextLine());
						System.out.print("Cognome : ");
						user.setSurname(input.nextLine());
						System.out.print("Indirizzo : ");
						user.setAddress(input.nextLine());
						System.out.print("Numero di telefono : ");
						user.setPhoneNumber(input.nextLine());
						if (Control.is_Valid_Phone_number(user.getPhoneNumber())){
							System.out.print("Codice fiscale : ");
							String fiscalCode=input.nextLine();
							user.setFiscalCode (fiscalCode);
							if (Control.is_Valid_Fiscal_code(user.getFiscalCode())){
								System.out.print("E-mail : ");
								String email = input.nextLine();
								user.setEmail(email);
								if(Control.is_Valid_email(user.getEmail())) {
									System.out.println("La password deve contenere minimo 8 caratteri e massimo 12 caratteri  ");
									System.out.println("La password deve contenere 2 numeri ");
									System.out.println("La password deve contenere 2 carattere in maiuscolo");
									System.out.println("La password deve contenere 1 carattere speciale tra �,$,%,&,?,�,!");
									System.out.print("Inserisci Password : ");
									String st =(input.nextLine());
									if (Control.is_Valid_Password(st)) {
										System.out.print("Conferma Password : ");
										user.setPassword(input.nextLine());
										if (user.getPassword().equals(st)){
											List<User> users = userRepository.getAll();
											for (User userReaded : users) {
												if (fiscalCode.equals(userReaded.getFiscalCode()) || email.equals(userReaded.getEmail())){
													System.out.println("Utente gi� registrato");
													registration =true;
												}
											}
											if (registration==false){
												userRepository.add(user);		
												System.out.println("Utente registrato");
											}	
										}
										else{
											System.out.println("Password non corrispondenti");
										}
									}
									else {
										System.out.println("La password non � valida " +st);
									}
								} 
								else {
									System.out.println("L'e-mail non � valida");
								}
							}
							else {
								System.out.println("Il codice fiscale inserito non � valido");
							}
						}	
						else {
							System.out.println("Numero di telefono non valido");
						}
						showLoginMenu();
					break;
					case "2":
						login();
					break;	
					case "3": 
						System.out.println("Programma terminato");
						System.exit(1);
					break;	
					default:
						System.out.println("Errore !!!");
						showLoginMenu();
				}
			}
		}
	}
	
	public static void login(){
		try (Scanner in = new Scanner(System.in)){
			boolean access=false;
			System.out.print("email: ");
			String email=in.nextLine();
			
			System.out.print("password: ");
			String password=in.nextLine();
			UserRepository userRepository = new MysqlUserRepository();
			List<User> users = userRepository.getAll();
			for(User userReaded : users) {
				if ( password.equals(userReaded.getPassword()) && email.equals(userReaded.getEmail()) && userReaded.isAdmin()==true) {
					System.out.println("Accesso amministratore effettuato ! Benvenuto " + userReaded.getName());
					Login.getInstance().setUserId(userReaded.getId());
					AdminMenu.showAdminMenu();
					access=true;
				}	
				if ( password.equals(userReaded.getPassword()) && email.equals(userReaded.getEmail()) && userReaded.isAdmin()==false) {
					System.out.println("Benvenuto " + userReaded.getName());
					Login.getInstance().setUserId(userReaded.getId());
					UserMenu.showUserMenu();
					access=true;
				}				
			}
			if (access==false){
				System.out.println("E-mail e password errati !");
				showLoginMenu();
			}
		}
	}
	
}
	

