package it.r8.righilibrary;
import java.sql.Date;
import java.util.List;
import java.util.Scanner;

import it.r8.righilibrary.RighiLibraryMain;
import it.r8.righilibrary.domain.Book;
import it.r8.righilibrary.domain.Lend;
import it.r8.righilibrary.domain.User;
import it.r8.righilibrary.repository.BookRepository;
import it.r8.righilibrary.repository.LendRepository;
import it.r8.righilibrary.repository.MysqlBookRepository;
import it.r8.righilibrary.repository.MysqlLendRepository;
import it.r8.righilibrary.repository.MysqlUserRepository;
import it.r8.righilibrary.repository.UserRepository;

public class UserMenu {
	
	public static void showUserMenu() {
		try (Scanner input = new Scanner(System.in)) {
			String c= null ;
			System.out.print("|      MAIN MENU        |\n"
			        +        "|_______________________|\n"
			        +		 "|     1 Libri           |\n"
					+ 		 "|     2 Area personale  |\n"
					+ 		 "|     3 Prestiti        |\n"
					+        "|     4 Logout          |\n"
					+		 "|_______________________|\n"); 
			
				c=input.nextLine();
				switch(c) {
					case "1": 
						BooksMenu();
					break;
					case "2":
						UserDataMenu();
					break;	
					case "3":
						lendMenu();
					break;
					case "4": 
						System.out.println("logout");
						RighiLibraryMain.showLoginMenu();
						break;	
					default:
						System.out.println("Errore !!!");
						showUserMenu();
				}
			
		}

	}

	public static void BooksMenu (){
		try (Scanner input = new Scanner (System.in)){
			BookRepository bookRepository = new MysqlBookRepository();
			String c=null;
			System.out.print("|          LIBRI          |\n"
					    +    "|_________________________|\n"
						+    "|     1 Visualizza        |\n"
			            +    "|     2 Ricerca           |\n"
			            +    "|     3 Menu principale   |\n"
			            +    "|_________________________|\n");
			c=input.nextLine();
			switch (c) {
				case "1" : 
					List<Book> b = bookRepository.getAll();
					for (Book bookReader : b){
						System.out.println(bookReader);
					}
					BooksMenu();
				break;
				case "2" :
					System.out.print("Titolo : ");
					List<Book> t = bookRepository.getbyTitle(input.nextLine());
					for (Book bookReader : t){
						System.out.println(bookReader);
					}
					BooksMenu();
				case "3" : 
					showUserMenu();
				default: 
					System.out.println("ERRORE");
					BooksMenu ();
				break;
			}
		}
	}
	public static void UserDataMenu(){
		try (Scanner input = new Scanner (System.in)){
			UserRepository userRepository = new MysqlUserRepository();
			User user = new User();
			String c =null;
			System.out.print("|       AREA PERSONALE       |\n"
						 +   "|____________________________|\n"
					     +   "|   1 Visualizza dati        |\n"
						 +   "|   2 Modifica dati          |\n"
						 +   "|   3 Disicrizione           |\n"
						 +   "|   4 Menu principale        |\n"
						 +   "|____________________________|\n");
			c=input.nextLine();
			switch(c){
				case "1" :
					User userReaded = userRepository.getById(Login.getInstance().getUserId());
					System.out.println(userReaded);  
					UserDataMenu();
				break;	
				case "2" :
					System.out.print("Indirizzo : ");
					user.setAddress(input.nextLine());
					System.out.print("Numero di telefono : ");
					user.setPhoneNumber(input.nextLine());
					System.out.print("E-mail : ");
					user.setEmail(input.nextLine());
					System.out.print("Password : ");
					user.setPassword(input.nextLine());
					userRepository.updateUser(Login.getInstance().getUserId(), user);
					UserDataMenu();
				break;
				case "3" :
					userRepository.delete(Login.getInstance().getUserId());
					System.out.println("Disiscrizione avvenuta con successo");
					RighiLibraryMain.showLoginMenu();
				break;
				case "4" :
					showUserMenu();
				break;
				default:
					System.out.println("Errore ");
					UserDataMenu();
			} 
			
			
			
		}
	}
	
	public static void lendMenu() {	
		try (Scanner input = new Scanner(System.in)) {
			LendRepository lendRepository = new MysqlLendRepository();
			BookRepository bookRepository = new MysqlBookRepository();
			Lend lend= new Lend();
			String c= "" ;
			System.out.print("|         Prestito            |\n"
					+		 "|_____________________________|\n"
					+		 "|     1 Prestito              |\n"
					+        "|     2 Restituzione          |\n"
					+ 		 "|     3 Visualizza            |\n"
					+        "|     4 Menu principale       |\n"
					+		 "|_____________________________|\n"); 
				c= input.nextLine();
				if (c.equals("1") || c.equals("3")) {
					
				}
				switch (c){
					case "1" :
						boolean bookReturn=true;
						Date date;
						Date date2;
						List<Lend> lends = lendRepository.getAll_byuser(Login.getInstance().getUserId());
						for(Lend lendReaded : lends) {
							date=lendReaded.getDateLoan();
							date.setMonth(date.getMonth()+1);
							date2=new Date(System.currentTimeMillis());
							if ( lendReaded.getDateReturn()==null &&  date2.after(date)) {
								System.out.println("Non hai ancora restituito: " + lendReaded);
								bookReturn=false;
							}		
						}
						if (bookReturn==true){
							System.out.print("id libro : ");  
							int borrow=input.nextInt();
							lend.setIdbook(borrow);
							lend.setIduser(Login.getInstance().getUserId());
							bookRepository.updateAmount(borrow);
							lendRepository.book_loan(lend);		
							System.out.println("Libro prestato");
						}
						lendMenu();
						
					break;	
					case "2" :
						System.out.print("id prestito : ");  
						int r=input.nextInt();
						lendRepository.book_return(r);
						List<Lend> lends3 = lendRepository.getAll_byuser(Login.getInstance().getUserId());
						for(Lend lendReaded : lends3) {
							if (lendReaded.getId()==r){
								System.out.println(lendReaded.getBook().getId());
								bookRepository.updateAmountReturn(lendReaded.getBook().getId());
							}
						}
						System.out.println("Libro restituito");
						lendMenu();
						
					case "3" : 
						List<Lend> lends2 = lendRepository.getAll_byuser(Login.getInstance().getUserId());
						for(Lend lendReaded : lends2) {
							System.out.println(lendReaded);
						}
						lendMenu();
					break;
					case "4": 
						showUserMenu();
					break;
					default:
						System.out.println("Errore !!!");
						lendMenu();
				}
		}
	}

}


	
