package it.r8.righilibrary;

public class Login {
	
	private static Login instance = null; //STIAMO UTLIZZANDO Design Pattern - Singleton Pattern
	
	private Integer userId;
	
	private Login() {}
	
	public static synchronized Login getInstance() {
		if (instance == null) {
			instance = new Login();
		}
		
		return instance;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
