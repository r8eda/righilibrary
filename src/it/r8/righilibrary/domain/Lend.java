package it.r8.righilibrary.domain;

import java.sql.Date;

public class Lend {
	private int id;
	private  Date dateLoan;
	private Date dateReturn ;
	private int idbook;
	private int iduser;
	private Book book;
	private User user;
	private boolean deleted;
	
	public Lend () {
		
	}
	public Lend(int id, Date dateLoan, Date dateReturn, Book book, User user) {
	
		this.id = id;
		this.dateLoan = dateLoan;
		this.dateReturn = dateReturn;
        this.book = book;
		this.user=user;
	}

	public String toString(){
		return "Prestito [Data prestito= " + dateLoan + ", Data ritorno= " + dateReturn +  ",  " + book + ", " + user + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDateLoan() {
		return dateLoan;
	}
	public void setDateLoan(Date dateLoan) {
		this.dateLoan = dateLoan;
	}
	public Date getDateReturn() {
		return dateReturn;
	}
	public void setDateReturn(Date dateReturn) {
		this.dateReturn = dateReturn;
	}
	public int getIdbook() {
		return idbook;
	}
	public void setIdbook(int idbook) {
		this.idbook = idbook;
	}
	public int getIduser() {
		return iduser;
	}
	public void setIduser(int iduser) {
		this.iduser = iduser;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}
