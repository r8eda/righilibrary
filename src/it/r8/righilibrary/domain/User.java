package it.r8.righilibrary.domain;

public class User {
	
	private int id;
	private String name;
	private String surname;
	private String phoneNumber;
	private String email;
	private String fiscalCode;
	private String address;
	private boolean deleted;
	private boolean admin;
	private String password;
	public User (){
		
	}
	
	public User (int id, String name, String surname, String phoneNumber, String email, String fiscalCode,String address, String password){
		this.id=id;
		this.name=name;
		this.surname=surname;
		this.phoneNumber=phoneNumber;
		this.email=email;
		this.fiscalCode=fiscalCode;
		this.address=address;
		this.password=password;
	}

	public String toString() {
		return "Utente [Codice fiscale= " + fiscalCode +", Nome= " + name +", Cognome= " + surname + ", Indirizzo= " + address + ", Numero di telefono= " + phoneNumber + ", E-mail= " + email + ", Password= " + password  + "]";
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getFiscalCode() {
		return fiscalCode;
	}
	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
	
	

	
	


