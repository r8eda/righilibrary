package it.r8.righilibrary;

public class Control {
	
	public static final int PASSWORD_LENGTH = 12; 
	public static boolean is_Valid_Password(String password) {

	        if (password.length() < 8 || password.length() > PASSWORD_LENGTH) return false;
	      

	        int charCount = 0;
	        int numCount = 0;
	        int specCount = 0;
	        for (int i = 0; i < password.length(); i++) {

	            char ch = password.charAt(i);

	            if (is_Numeric(ch)) numCount++;
	            else if (is_Letter(ch)) charCount++;
	            else if  (is_Special(ch)) specCount++ ;
	            else return false;
	        }


	        return (charCount >= 1 && numCount >= 1 && specCount >=1);
	    }
	 

	    public static boolean is_Letter(char ch) {
	        ch = Character.toUpperCase(ch);
	        return (ch >= 'A' && ch <= 'Z');
	    }

	    public static boolean is_Numeric(char ch) {
	    	return (ch >='0' && ch <='9'); 
	    	
	    }
	    
	    public static boolean is_Special(char ch){
	    	
	    	return (ch== '�' ||ch== '$' ||ch == '%' || ch== '&' ||ch== '?' || ch== '�' || ch== '!' );

	    }
	    
	    public static boolean is_Valid_email(String email) {
	    	int snailCount =0;
	    	int pointCount =0;
	    	int letterCount =0;
	    	int numCount=0;
	    	
	    	for (int i = 0; i < email.length();i++) {
	    		char ch = email.charAt(i);
	    		if (is_Point(ch)) pointCount++;
	    		else if (is_Snail(ch)) snailCount++;
	    		else if (is_Letter(ch)) letterCount ++;
	    		else if (is_Numeric(ch)) numCount ++;
	    		else return false;
	    	}
	    	return (pointCount < 4  && snailCount < 2 && letterCount >4 && numCount >-1);
	    
	    }
	    
	    public static boolean is_Point(char ch) {
	    	
	    	return(ch =='.');
	    }
	    
	    public static boolean is_Snail(char ch) {
	    	
	    	return(ch =='@');
	    }
	    
	    public static boolean is_Valid_Phone_number(String Phone_number) {
	    	int Phone_numberCount =0;
	    	
	    	
	    	
	    	for (int i = 0; i< Phone_number.length();i++) {
	    		char ch = Phone_number.charAt(i);
	    		if (is_Valid_Phone_number(ch)) Phone_numberCount++;
	    		else return false;
	    		
	    	}
	    	return Phone_numberCount >= 1;
	    
	    	
	    }
	    public static boolean is_Valid_Phone_number(char ch) {
	    	return (ch >='0' && ch <='9'); 
	    }
            public static final int Fiscal_code_length =16;
            public static boolean is_Valid_Fiscal_code(String Fiscal_code){
            	if (Fiscal_code.length()<16 || Fiscal_code.length()> Fiscal_code_length) return false;
            	 int charCount=0;
            	 int numCount=0;
            	 for (int i=0; i< Fiscal_code.length(); i++){
            		 char ch = Fiscal_code.charAt(i);
            		 if (is_Numeric(ch)) numCount ++;
            		 else if (is_Letter(ch)) charCount ++;
            		 else return false;
            		 
            	 }
                  return (charCount>=9 && numCount>= 7 );
                  
            }
        
         
     	    	
     	    }
            
            

	



