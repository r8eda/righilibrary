package it.r8.righilibrary.repository;

import java.util.List;

import it.r8.righilibrary.domain.Book; 
	public interface BookRepository {
		List<Book> getAll();
		List<Book> getbyTitle(String title);
		void add (Book book);
		void update(int id, Book book);
		void delete(int id);
		void updateAmount(int id);
		void updateAmountReturn(int id);
	}



