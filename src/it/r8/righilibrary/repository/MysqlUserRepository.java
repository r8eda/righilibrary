package it.r8.righilibrary.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.r8.righilibrary.domain.User;

public class MysqlUserRepository implements UserRepository {
	private static final String URL = "jdbc:mysql://localhost:3306/righilibrary?autoReconnect=true&useSSL=false";
	private static final String USER = "root";
	private static final String PASSWORD = "test";
	/*INSERT*/
	public void add(User user) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format (Locale.US,"INSERT INTO User (name, surname, phone_number, email, fiscal_code, address, password) " + 
										  "VALUES ('%s', '%s', '%s','%s','%s','%s','%s')", user.getName(),user.getSurname(), user.getPhoneNumber(), user.getEmail(), user.getFiscalCode(), user.getAddress(), user.getPassword());
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}	
	
	public List<User> getAll() {
		List<User> users = new ArrayList<User>();
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
		    ResultSet resultSet = statement.executeQuery("SELECT * FROM righilibrary.user Where deleted =0");
		    while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setPassword(resultSet.getString("password"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setPhoneNumber(resultSet.getString("phone_number"));
				user.setEmail(resultSet.getString("email"));
				user.setFiscalCode(resultSet.getString("fiscal_code"));
				user.setAddress(resultSet.getString("address"));
				user.setAdmin(resultSet.getBoolean("admin"));
				users.add(user);
			}
		}    
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
		return users;
	}
	
	public User getById(int id) {
		User user = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("SELECT * FROM User u WHERE u.id = %d", id);
			
			ResultSet resultSet = statement.executeQuery(sql);
			
			if (resultSet.first()) {
				user = new User();
				user.setId(resultSet.getInt("id"));
				user.setPassword(resultSet.getString("password"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setPhoneNumber(resultSet.getString("phone_number"));
				user.setEmail(resultSet.getString("email"));
				user.setFiscalCode(resultSet.getString("fiscal_code"));
				user.setAddress(resultSet.getString("address"));
				user.setAdmin(resultSet.getBoolean("admin"));
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return user;
	}
	
	public void update(int id, User user) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE User u" +
									   "  SET name = '%s'," +
									   "	  surname = '%s'," +
									   "      phone_number = '%s'," +
									   "      email = '%s'," +
									   "      fiscal_code = '%s'," +
									   "      address = '%s'," +
									   "      password = '%s'" +
									   "WHERE u.id = %d", user.getName(),
										   user.getSurname(), user.getPhoneNumber(), user.getEmail(),
										   user.getFiscalCode(), user.getAddress(), user.getPassword(), id);
						
			statement.executeUpdate(sql);
						
		} 
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}	
	}	
	
	public void privilege(int id) {
		User user = new User ();
		user.setAdmin(false);
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE User SET admin = 1 WHERE id = %d" ,id);
			int result = statement.executeUpdate(sql);
			System.out.println(result);
							
		}
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}	
	
	public void delete(int id) {
		User user = new User ();
		user.setDeleted(false);
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE User SET deleted =1 WHERE id =%d " ,id);
			int result = statement.executeUpdate(sql);
			System.out.println(result);
							
		}
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	public void updateUser(int id, User user) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE User u" +
									   "  SET phone_number = '%s'," +
									   "      email = '%s'," +
									   "      address = '%s'," +
									   "      password = '%s'" +
									   "WHERE u.id = %d",  user.getPhoneNumber(), user.getEmail(), user.getAddress(), user.getPassword(), id);
						
			statement.executeUpdate(sql);
						
		} 
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}	
	}	
}


		   	
		   	
		   	
