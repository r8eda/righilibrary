package it.r8.righilibrary.repository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.r8.righilibrary.domain.Lend;
import it.r8.righilibrary.domain.User;
import it.r8.righilibrary.domain.Book;


	public class MysqlLendRepository implements LendRepository{
	private static final String URL = "jdbc:mysql://localhost:3306/righilibrary?autoReconnect=true&useSSL=false";
	private static final String USER = "root";
	private static final String PASSWORD = "test";
	
	public void book_loan(Lend lend) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format (Locale.US,"INSERT INTO Lend(date_loan , idbook, iduser) " + 
										  "VALUES (current_date(), '%d', '%d')" ,
										   lend.getIdbook(),lend.getIduser());
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}	
	
	public void book_return(int id) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format(Locale.US,"UPDATE Lend SET date_return = current_date() WHERE id = %d",id);
	    			
			statement.executeUpdate(sql);
			}
			catch (SQLException e) {
	    		System.out.println("SQL exception");
	    		e.printStackTrace();
	    	}
	}
		
	public List<Lend> getAll() {
	   	List<Lend> lends = new ArrayList<Lend>();
	   	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
	   		Statement statement = connection.createStatement();
		    ResultSet resultSet = statement.executeQuery("SELECT lend.*, user.*, book.* "
		    		+ "FROM lend inner join user on lend.iduser=user.id inner join book on lend.idbook=book.id "
		    		+ "WHERE lend.deleted=0");
			    
		    while (resultSet.next()) {
				  
			    User user = new User();
			    Book book= new Book();
			    Lend lend = new Lend();
			    book.setId(resultSet.getInt("book.id"));
			    book.setAmount(resultSet.getInt("amount"));
				book.setIsbn(resultSet.getString("isbn"));
				book.setTitle(resultSet.getString("title"));
				book.setWriter(resultSet.getString("writer"));
				book.setPublisher(resultSet.getString("publisher"));
				book.setPosition(resultSet.getString("position"));
						
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setAddress(resultSet.getString("Address"));
				user.setFiscalCode(resultSet.getString("fiscal_code"));
				user.setPhoneNumber(resultSet.getString("phone_number"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
					
			    lend.setId(resultSet.getInt("lend.id"));
			    lend.setDateLoan(resultSet.getDate("lend.date_loan"));
			    lend.setDateReturn(resultSet.getDate("lend.date_return"));
			    lend.setBook(book);
			    lend.setUser(user);
			    lends.add(lend);
				
		   } 
	   }
	   catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
	   }
	   return lends;
	}
	
	
	public List<Lend> getAll_byuser(int id) {
	   	List<Lend> lends = new ArrayList<Lend>();
	   	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
	   		Statement statement = connection.createStatement();
	   		String sql = String.format("SELECT lend.*, user.*, book.* "
		    		+ "FROM lend inner join user on lend.iduser=user.id inner join book on lend.idbook=book.id "
		    		+ "WHERE lend.deleted=0 AND lend.iduser= %d ", id);
	   		ResultSet resultSet = statement.executeQuery(sql);    
		    while (resultSet.next()) {
				  
		    	User user = new User();
			    Book book= new Book();
			    Lend lend = new Lend();
			    book.setId(resultSet.getInt("book.id"));
			    book.setAmount(resultSet.getInt("amount"));
				book.setIsbn(resultSet.getString("isbn"));
				book.setTitle(resultSet.getString("title"));
				book.setWriter(resultSet.getString("writer"));
				book.setPublisher(resultSet.getString("publisher"));
				book.setPosition(resultSet.getString("position"));
						
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setAddress(resultSet.getString("Address"));
				user.setFiscalCode(resultSet.getString("fiscal_code"));
				user.setPhoneNumber(resultSet.getString("phone_number"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
					
			    lend.setId(resultSet.getInt("lend.id"));
			    lend.setDateLoan(resultSet.getDate("lend.date_loan"));
			    lend.setDateReturn(resultSet.getDate("lend.date_return"));
			    lend.setBook(book);
			    lend.setUser(user);
			    lends.add(lend);
				
		   } 
	   }
	   catch (SQLException e) {
			System.out.println("SQL exception");
	   }
	   return lends;
	}
	
	public void update(int id, Lend lend) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Lend l" +
	    							   "  SET date_loan = '%s'," +
	    							   "	  date_return = '%s'," +
	    							   "	  idbook = '%d'," +
	    							   "      iduser = '%d'," +	
	    							   "WHERE l.id = %d", lend.getDateLoan(),
	    							   lend.getDateReturn(),lend.getIdbook(), lend.getIduser(), id);
	    			
			statement.executeUpdate(sql);
			}
			catch (SQLException e) {
	    		System.out.println("SQL exception");
	    		e.printStackTrace();
	    	}
	    }
	    	/*DELETE*/
	public void delete(int id) {
	   	Lend lend = new Lend();
	   	lend.setDeleted(false);
	   	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
	   		Statement statement = connection.createStatement();
	   		String sql = String.format("UPDATE Lend SET deleted =1 WHERE id =%d", id);
	   		int result = statement.executeUpdate(sql);
	   		System.out.println(result);
	    			
	   	} 
	   	catch (SQLException e) {
	   		System.out.println("SQL exception");
	   		e.printStackTrace();
	   	}
	}
}