package it.r8.righilibrary.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.r8.righilibrary.domain.Book;



public class MysqlBookRepository implements BookRepository {

	private static final String URL = "jdbc:mysql://localhost:3306/righilibrary?autoReconnect=true&useSSL=false";
	private static final String USER = "root";
	private static final String PASSWORD = "test";
	
	/*INSERT*/
	public void add(Book book) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format(Locale.US,"INSERT INTO Book( amount, isbn, title, writer, publisher, position) " + 
										  "VALUES ('%d', '%s', '%s','%s', '%s','%s' )" ,book.getAmount(),
										  book.getIsbn(), book.getTitle(), book.getWriter(), book.getPublisher(),book.getPosition() );
			statement.executeUpdate(sql);
			}
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
		}
	
	
	
	/*SELECT*/
	public List<Book> getAll() {
		List<Book> books = new ArrayList<Book>();
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Book WHERE deleted=0");
			while (resultSet.next()) {
				Book book = new Book();
				book.setId(resultSet.getInt("id"));
			    book.setAmount(resultSet.getInt("amount"));
				book.setIsbn(resultSet.getString("isbn"));
				book.setTitle(resultSet.getString("title"));
				book.setWriter(resultSet.getString("writer"));
				book.setPublisher(resultSet.getString("publisher"));
				book.setPosition(resultSet.getString("position"));
				
				books.add(book);
			}
		}
		catch (SQLException e) {
			System.out.println("SQL exception");
		}

		return books;
	}
	
	public List<Book> getbyTitle(String title) {
		List<Book> books = new ArrayList<Book>();
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("SELECT * FROM Book WHERE deleted=0 AND title='%s'", title);
			ResultSet resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				Book book = new Book();
				book.setId(resultSet.getInt("id"));
			    book.setAmount(resultSet.getInt("amount"));
				book.setIsbn(resultSet.getString("isbn"));
				book.setTitle(resultSet.getString("title"));
				book.setWriter(resultSet.getString("writer"));
				book.setPublisher(resultSet.getString("publisher"));
				book.setPosition(resultSet.getString("position"));
				
				books.add(book);
			}
		}
		catch (SQLException e) {
			System.out.println("SQL exception");
		}

		return books;
	}
	
	/*UPDATE*/
	public void update(int id, Book book) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Book b" +
									   "  SET amount = '%d'," +
									   "	 isbn = '%s'," +
									   "      title = '%s'," +
									   "      writer = '%s'," +
									   "      publisher = '%s'," +
									   "      position = '%s'" +
									   "WHERE b.id = %d", book.getAmount(),
									  book.getIsbn(), book.getTitle(),  book.getWriter(),book.getPublisher(),book.getPosition(), id);
			statement.executeUpdate(sql);
		}
	    catch (SQLException e) {
	    	System.out.println("SQL exception");
			e.printStackTrace();
			}
		}
	
	
	
	/*DELETE*/
	public void delete(int id) {
		Book book = new Book();
		book.setDeleted(false);
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Book SET deleted =1 WHERE id =%d", id);
			
			int result = statement.executeUpdate(sql);
			System.out.println(result);
			}
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	
	public void updateAmount(int id) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Book b  SET amount = amount-1 WHERE b.id = %d", id);
			statement.executeUpdate(sql);
		}
	    catch (SQLException e) {
	    	System.out.println("SQL exception");
			e.printStackTrace();
			}
		}
	
	public void updateAmountReturn(int id) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Book b  SET amount = amount+1 WHERE b.id = %d", id);
			statement.executeUpdate(sql);
		}
	    catch (SQLException e) {
	    	System.out.println("SQL exception");
			e.printStackTrace();
			}
		}
}





