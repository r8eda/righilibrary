package it.r8.righilibrary.repository;

import java.util.List;
import it.r8.righilibrary.domain.Lend;
	


public interface LendRepository {

		List<Lend> getAll();
		
		List<Lend> getAll_byuser(int id);
				
		void book_loan(Lend lend);
		
		void book_return(int id);
			
		void update(int id, Lend lend);
			
		void delete(int id);
}


