package it.r8.righilibrary.repository;

import java.util.List;

import it.r8.righilibrary.domain.User;

public interface UserRepository {
	List<User> getAll();
	void add (User user);
	void update(int id, User user);
	void delete(int id);
	void privilege(int id);
	User getById(int id);
	void updateUser(int id, User user);
}
